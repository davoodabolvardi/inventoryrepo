# README #


### این ریپو چیست؟? ###

* این ریپو برای یک پروژه تعیین سطح هست. پروژه ای که میتوان با آن کالاها را ثبت کرد و موجودی کالاهای انبار را با امکان فیلتر کردن و مرتب سازی فهرست بر اساس کد کالا، نام کالا، گروه کالا، موجودی، حداقل موجودی و وضعیت را مشاهده کرد. و همچنین میتوان ورود و فروش کالاها را از انبار را ثبت کرد   
* Version 1.0


HTTPS:
```bash
git clone https://davoodabolvardi@bitbucket.org/davoodabolvardi/inventoryrepo.git
```

### اقدامات لازم قبل از اجرا پروژه? ###
#### Update-database
* Open Package Manager Console and Change Default Project from Inventory.WebAPI to Inventory.WebAPI.DataLayer
* Type the following command and press Enter

```bash
update-database
```
* If Done, the database has been successfully created and updated.

**Note**: برای تعریف کالا، ابتدا باید نوع کالا

category

.را وارد کرد


### ساختار ریپو

ساختار این ریپو به شرح زیر می باشد:

``` bash
├──Inventoryrepo
├────Inventory
├──────Inventory.WebApi
├──────Inventory.WebAPI.Contracts
├──────Inventory.WebAPI.DataLayer
├──────Inventory.WebAPI.Extensions
├──────Inventory.WebAPI.Supports
├──────Inventory.WebAPI.Validations
```


### ضمیمه ریپو ###

* یک کالکشن پستمن هم ضمیمه این ریپو درنظر گرفته شده و لازمه بسته به اطلاعاتی که وارد پایگاه داده میشود مقادیر تغییر یابد 