﻿using FluentMigrator.Runner;
using FluentValidation;
using Inventory.WebAPI.Contracts.EntryBill;
using Inventory.WebAPI.Contracts.Product;
using Inventory.WebAPI.Contracts.SaleBill;
using Inventory.WebAPI.DataLayer.Migrations;
using Inventory.WebAPI.Validations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace Inventory.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureApiVersioning(this IServiceCollection services)
        {
            services.AddApiVersioning(options => options.ReportApiVersions = true);
        }

        public static void ConfigureCORS(this IServiceCollection services)
        {
            services.AddCors(c => c.AddPolicy("AllowOrigin", builder =>
              {
                  builder.WithOrigins("*")
                  .WithMethods("GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS")
                  .AllowAnyHeader();
              }));
        }

        public static void ConfigureValidators(this IServiceCollection services)
        {
            services.AddTransient<IValidator<ProductBody>, ProductBodyValidator>();
            services.AddTransient<IValidator<EntryBillBody>, EntryBillValidator>();
            services.AddTransient<IValidator<SaleBillBody>, SaleBillValidator>();
        }

        public static void ConfigureFluentMigrator(this IServiceCollection services)
        {
            services.AddLogging(c => c.AddFluentMigratorConsole())
                .AddFluentMigratorCore()
                .ConfigureRunner(c => c
                .AddSqlServer2016()
                .WithGlobalConnectionString("Persist Security Info = False; Integrated Security = true; Initial Catalog = Inventory; server =.")
                .ScanIn(typeof(Migration_20200104103311).Assembly).For.All());
        }
    }
}
