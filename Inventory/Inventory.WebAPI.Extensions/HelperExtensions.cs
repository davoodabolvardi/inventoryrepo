﻿using FluentValidation;
using Inventory.WebAPI.Contracts.Product;
using Inventory.WebAPI.DataLayer.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Inventory.WebAPI.Extensions
{
    public static class HelperExtensions
    {

        public static string GetErrorMessages(this ValidationException ex)
        {
            var errorMessages = "";
            foreach (var error in ex.Errors)
            {
                errorMessages += $"{error.ErrorMessage}, ";
            }
            errorMessages = errorMessages.Substring(0, errorMessages.Length - 2);

            return errorMessages;
        }

        public static int GenerateEntryBillNumber(this IEnumerable<EntryBill> entryBills)
        {
            if (entryBills.Count() > 0)
            {
                var latestEntryBillNumber = entryBills.Max(z => z.EntryBillNumber);
                return latestEntryBillNumber + 1;
            }
            return 10000;
        }

        public static int GenerateNumber(this IEnumerable<SaleBill> saleBills, bool isSaleNumber = false)
        {
            if (saleBills.Count() > 0)
            {
                if (isSaleNumber)
                {
                    var latestSaleBillNumber = saleBills.Max(z => z.SaleBillNumber);
                    return latestSaleBillNumber + 1;
                }
                var latestAccountingNumber = saleBills.Max(z => z.AccountingNumber);
                return latestAccountingNumber + 1;
            }
            return 10000;
        }

        public static List<InventoryItem> InventoryFiltering(this IEnumerable<InventoryItem> inventories, InventoryListQuery inventoryListQuery)
        {
            var inventoryItems = new List<InventoryItem>();
            var filteringCombileFlag = false;

            inventoryItems = inventories.ToList();

            if (inventoryListQuery.ProductId != null)
            {
                inventoryItems = inventories.Where(z => z.ProductId.ToString().Contains(inventoryListQuery.ProductId.ToString())).ToList();

                filteringCombileFlag = true;
            }

            if (inventoryListQuery.ProductTitle != null)
            {
                if (filteringCombileFlag)
                    inventoryItems = inventoryItems.Where(z => z.ProductTitle.ToString().Contains(inventoryListQuery.ProductTitle)).ToList();
                else
                    inventoryItems = inventories.Where(z => z.ProductTitle.ToString().Contains(inventoryListQuery.ProductTitle)).ToList();

                filteringCombileFlag = filteringCombileFlag ? true : false;
            }

            if (inventoryListQuery.CategoryName != null)
            {
                if (filteringCombileFlag)
                    inventoryItems = inventoryItems.Where(z => z.CategoryName.ToString().Contains(inventoryListQuery.CategoryName)).ToList();
                else
                    inventoryItems = inventories.Where(z => z.CategoryName.ToString().Contains(inventoryListQuery.CategoryName)).ToList();

                filteringCombileFlag = filteringCombileFlag ? true : false;
            }

            if (inventoryListQuery.Inventory != null)
            {
                if (filteringCombileFlag)
                    inventoryItems = inventoryItems.Where(z => z.Inventory.ToString().Contains(inventoryListQuery.Inventory.ToString())).ToList();
                else
                    inventoryItems = inventories.Where(z => z.Inventory.ToString().Contains(inventoryListQuery.Inventory.ToString())).ToList();

                filteringCombileFlag = filteringCombileFlag ? true : false;
            }

            if (inventoryListQuery.MinimumInventory != null)
            {
                if (filteringCombileFlag)
                    inventoryItems = inventoryItems.Where(z => z.MinimumInventory.ToString().Contains(inventoryListQuery.MinimumInventory.ToString())).ToList();
                else
                    inventoryItems = inventories.Where(z => z.MinimumInventory.ToString().Contains(inventoryListQuery.MinimumInventory.ToString())).ToList();

                filteringCombileFlag = filteringCombileFlag ? true : false;
            }

            if (inventoryListQuery.Status != null)
            {
                if (filteringCombileFlag)
                    inventoryItems = inventoryItems.Where(z => z.Status.ToString().Contains(inventoryListQuery.Status.ToString())).ToList();
                else
                    inventoryItems = inventories.Where(z => z.Status.ToString().Contains(inventoryListQuery.Status.ToString())).ToList();

                filteringCombileFlag = filteringCombileFlag ? true : false;
            }


            return inventoryItems;
        }

        public static List<InventoryItem> InventorySorting(this IEnumerable<InventoryItem> inventories, InventoryListQuery inventoryListQuery)
        {
            var inventoryItems = new List<InventoryItem>();

            switch (inventoryListQuery.WhichOneToSort.ToUpper())
            {
                case "PRODUCTID":
                    if (!inventoryListQuery.Descending)
                        inventoryItems = inventories.OrderBy(x => x.ProductId).ToList();
                    else
                        inventoryItems = inventories.OrderByDescending(x => x.ProductId).ToList();

                    break;

                case "PRODUCTTITLE":
                    if (!inventoryListQuery.Descending)
                        inventoryItems = inventories.OrderBy(x => x.ProductTitle).ToList();
                    else
                        inventoryItems = inventories.OrderByDescending(x => x.ProductTitle).ToList();

                    break;

                case "CATEGORYNAME":
                    if (!inventoryListQuery.Descending)
                        inventoryItems = inventories.OrderBy(x => x.CategoryName).ToList();
                    else
                        inventoryItems = inventories.OrderByDescending(x => x.CategoryName).ToList();

                    break;

                case "INVENTORY":
                    if (!inventoryListQuery.Descending)
                        inventoryItems = inventories.OrderBy(x => x.Inventory).ToList();
                    else
                        inventoryItems = inventories.OrderByDescending(x => x.Inventory).ToList();

                    break;

                case "MINIMUMINVENTORY":
                    if (!inventoryListQuery.Descending)
                        inventoryItems = inventories.OrderBy(x => x.MinimumInventory).ToList();
                    else
                        inventoryItems = inventories.OrderByDescending(x => x.MinimumInventory).ToList();

                    break;

                case "STATUS":
                    if (!inventoryListQuery.Descending)
                        inventoryItems = inventories.OrderBy(x => x.Status).ToList();
                    else
                        inventoryItems = inventories.OrderByDescending(x => x.Status).ToList();

                    break;

                default:
                    inventoryItems = inventories.ToList();
                    break;
            }

            return inventoryItems;
        }

        public static List<InventoryItem> InventoryPagination(this IEnumerable<InventoryItem> inventories, InventoryListQuery inventoryListQuery)
        {
            var inventoryItems = new List<InventoryItem>();

            inventoryItems = inventories.ToList();

            var count = inventories.Count();

            var pagesCount = count / inventoryListQuery.PageSize;
            var remaining = count % inventoryListQuery.PageSize;

            if (remaining > 0)
                pagesCount++;

            if (inventoryListQuery.Page > pagesCount)
            {
                return null;
            }

            inventoryItems = inventories
                .Skip((inventoryListQuery.Page - 1) * inventoryListQuery.PageSize)
                .Take(inventoryListQuery.PageSize)
                .ToList();

            return inventoryItems;
        }
    }
}
