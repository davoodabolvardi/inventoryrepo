using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.UnitOfWorks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Inventory.WebAPI.Extensions;
using Inventory.WebAPI.DataLayer.Migrations;
using FluentMigrator.Runner.Initialization;

namespace Inventory.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration["ConnectionString:Connection"];

            services.AddDbContext<AppDbContext>(contextBuilder => contextBuilder.UseSqlServer(connectionString));

            services.ConfigureValidators();

            services.ConfigureCORS();

            services.AddTransient<UnitOfWork>();

            services.ConfigureApiVersioning();



            services.AddControllers();

            services.ConfigureFluentMigrator();

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCors("AllowOrigin");
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            Database.EnsureDatabase("Persist Security Info = False; Integrated Security = true; Initial Catalog = master; server = .", "Inventory");

            app.Migrate();
        }
    }
}
