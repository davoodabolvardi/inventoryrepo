﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Inventory.WebAPI.Contracts.SaleBill;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.UnitOfWorks;
using Inventory.WebAPI.Extensions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Inventory.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/inventory/v{version:apiVersion}/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class SaleBillController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly ILogger<SaleBillController> _logger;
        private readonly IValidator<SaleBillBody> _saleBillValidator;

        public SaleBillController(UnitOfWork unitOfWork, ILogger<SaleBillController> logger, IValidator<SaleBillBody> saleBillValidator)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _saleBillValidator = saleBillValidator;
        }

        [MapToApiVersion("1.0")]
        [HttpPost]
        public IActionResult Insert([FromBody] SaleBillBody saleBillBody)
        {
            if (saleBillBody == null)
                return BadRequest(new { message = "SaleBillBody must not be null" });
            try
            {
                _saleBillValidator.ValidateAndThrow(saleBillBody);


                bool saleBillInserted = false;
                int salebillId = 0;
                var saleBillView = new List<SaleBillView>();


                foreach (var item in saleBillBody.SaleBillDetailBodies)
                {
                    if (_unitOfWork.ProductRepository.Get(x => x.Id == item.ProductId).Count() > 0)
                    {
                        var saleBillProduct = _unitOfWork.ProductRepository.MinimumInventoryIsValid(item.ProductId, item.Count);
                        if (saleBillProduct.MinInventoryIsValid)
                        {
                            if (!saleBillInserted)
                            {
                                var saleBill = _unitOfWork.SaleBillRepository.Insert(new SaleBill()
                                {
                                    CustomerName = saleBillBody.CustomerName,
                                    AccountingNumber = _unitOfWork.SaleBillRepository.Get().GenerateNumber(false),
                                    SaleBillNumber = _unitOfWork.SaleBillRepository.Get().GenerateNumber(true),
                                });
                                saleBillInserted = true;
                                salebillId = saleBill.Id;
                            }
                            if (saleBillInserted)
                            {
                                var saleBillDetail = _unitOfWork.SaleBillDetailRepository.Insert(new SaleBillDetail()
                                {
                                    SaleBillId = salebillId,
                                    ProductId = item.ProductId,
                                    Count = item.Count,
                                    Amount = item.Amount
                                });
                                saleBillView.Add(new SaleBillView()
                                {
                                    ProductName = saleBillProduct.ProductTitle,
                                    Message = "Successfull"
                                });
                            }
                        }
                        else
                        {
                            saleBillView.Add(new SaleBillView()
                            {
                                ProductName = saleBillProduct.ProductTitle,
                                Message = "UnSuccessful - Your count as requested more than minimum inventory"
                            });
                        }
                    }
                }
                if (saleBillView.Count == 0)
                    return BadRequest(new { message = "No valid products were registered for this SaleBill" });

                return Ok(saleBillView);
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Insert - SaleBill: {ex.GetErrorMessages()}");
                return BadRequest(new { message = ex.GetErrorMessages() });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Insert - SaleBill: {ex.Message}");
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
