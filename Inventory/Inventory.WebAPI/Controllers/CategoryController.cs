﻿using System;
using System.Linq;
using Inventory.WebAPI.Contracts.Category;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.UnitOfWorks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Inventory.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/inventory/v{version:apiVersion}/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class CategoryController : ControllerBase
    {

        private readonly UnitOfWork _unitOfWork;
        private readonly ILogger<CategoryController> _logger;
        public CategoryController(UnitOfWork unitOfWork, ILogger<CategoryController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        [MapToApiVersion("1.0")]
        [HttpPost]
        public IActionResult INSERT([FromBody] CategoryBody categoryBody)
        {
            try
            {
                if (categoryBody == null)
                {
                    return BadRequest(new { message = "CategoryBody can not be null or empty." });
                }
                var categoreis = _unitOfWork.CategoryRepository.Get(x => x.Title == categoryBody.Title.Trim());
                if (categoreis.Count() > 0)
                {
                    return Conflict(new { message = "This category exists" });
                }
                var category = new Category()
                {
                    Title = categoryBody.Title.Trim()
                };
                var newCategory = _unitOfWork.CategoryRepository.Insert(category);

                return Ok(new { id = newCategory.Id });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Insert new category: {ex.Message}");
                return BadRequest(new { message = ex.Message });
            }
        }

        [MapToApiVersion("1.0")]
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var categoreis = _unitOfWork.CategoryRepository.Get()
                    .Select(x=> new 
                    {
                        id = x.Id,
                        title = x.Title
                    }).ToList();

                return Ok(categoreis);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Get all categories: {ex.Message}");
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}
