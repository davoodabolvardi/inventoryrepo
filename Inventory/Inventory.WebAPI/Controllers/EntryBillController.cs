﻿using System;
using System.Linq;
using FluentValidation;
using Inventory.WebAPI.Contracts.EntryBill;
using Inventory.WebAPI.DataLayer.UnitOfWorks;
using Inventory.WebAPI.Extensions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Inventory.WebAPI.DataLayer.Entities;

namespace Inventory.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/inventory/v{version:apiVersion}/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class EntryBillController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly ILogger<EntryBillController> _logger;
        private readonly IValidator<EntryBillBody> _entryBillValidator;
        public EntryBillController(UnitOfWork unitOfWork, ILogger<EntryBillController> logger, IValidator<EntryBillBody> entryBillValidator)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _entryBillValidator = entryBillValidator;
        }

        [HttpPost]
        [MapToApiVersion("1.0")]
        public IActionResult Insert([FromBody] EntryBillBody entryBillBody)
        {
            if (entryBillBody == null)
                return BadRequest(new { message = "EntryBillBody can not be null or empty" });
            try
            {
                _entryBillValidator.ValidateAndThrow(entryBillBody);

                var entryBill = _unitOfWork.EntryBillRepository.Insert(new EntryBill()
                {
                    EntryBillNumber = _unitOfWork.EntryBillRepository.Get().GenerateEntryBillNumber()
                });
                foreach (var item in entryBillBody.EntryBillDetails)
                {
                    if (_unitOfWork.ProductRepository.Get(x => x.Id == item.ProductId).Count() > 0)
                    {
                        _unitOfWork.EntryBillDetailRepository.Insert(new EntryBillDetail()
                        {
                            EntryBillId = entryBill.Id,
                            ProductId = item.ProductId,
                            Count = item.Count
                        });
                    }
                }

                return Ok();
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Insert - EntryBill: {ex.GetErrorMessages()}");
                return BadRequest(new { message = ex.GetErrorMessages() });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Insert - EntryBill: {ex.Message}");
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}
