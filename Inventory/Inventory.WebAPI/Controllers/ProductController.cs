﻿using System;
using System.Linq;
using FluentValidation;
using Inventory.WebAPI.Contracts.Product;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.UnitOfWorks;
using Inventory.WebAPI.Extensions;
using Inventory.WebAPI.Validations;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Inventory.WebAPI.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/inventory/v{version:apiVersion}/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class ProductController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly ILogger<ProductController> _logger;
        private readonly IValidator<ProductBody> _productBodyValidator;

        public ProductController(UnitOfWork unitOfWork, ILogger<ProductController> logger, IValidator<ProductBody> productBodyValidator)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _productBodyValidator = productBodyValidator;
        }

        [HttpPost]
        [MapToApiVersion("1.0")]
        public IActionResult Insert([FromBody] ProductBody productBody)
        {
            if (productBody == null)
            {
                return BadRequest(new { message = "Productbody can not be null" });
            }
            try
            {
                _productBodyValidator.ValidateAndThrow(productBody);

                var prod = _unitOfWork.ProductRepository.Get(c => c.CategoryId == productBody.CategoryId && c.Title == productBody.Title.Trim());

                if (prod.Count() > 0)
                    return Conflict(new { message = "This name has already been registered for this category." });


                var product = _unitOfWork.ProductRepository.Insert(new Product()
                {
                    CategoryId = productBody.CategoryId,
                    Title = productBody.Title,
                    MinimumInventory = productBody.MinimumInventory
                });

                return Ok(new { id = product.Id });
            }
            catch (ValidationException ex)
            {
                _logger.LogError($"Insert new product: {ex.GetErrorMessages()}");
                return BadRequest(new { message = ex.GetErrorMessages() });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Insert new product: {ex.Message}");
                return BadRequest(new { message = ex.Message });
            }
        }

        [MapToApiVersion("1.0")]
        [HttpGet]
        public IActionResult InventoryList([FromQuery] InventoryListQuery inventoryListQuery)
        {
            try
            {
                var QueryValidator = inventoryListQuery.InventoryListQueryValidator();
                if (QueryValidator.Count > 0)
                    return BadRequest(QueryValidator);

                var products = _unitOfWork.ProductRepository.Get();

                var inventoryListView = new InventoryListView();

                foreach (var item in products)
                {
                    var inventoryItem = new InventoryItem
                    {
                        ProductId = item.Id,
                        ProductTitle = item.Title,
                        Status = _unitOfWork.ProductRepository.GetProductStatus(item.Id),
                        MinimumInventory = item.MinimumInventory,
                        Inventory = _unitOfWork.ProductRepository.CalculateInventory(item.Id),
                        CategoryName = _unitOfWork.ProductRepository.GetCategoryName(item.Id)
                    };
                    inventoryListView.Inventories.Add(inventoryItem);
                }

                //filtering
                inventoryListView.Inventories = inventoryListView.Inventories.InventoryFiltering(inventoryListQuery);

                //sorting
                if (inventoryListQuery.WhichOneToSort != null)
                    inventoryListView.Inventories = inventoryListView.Inventories.InventorySorting(inventoryListQuery);

                //count in the response
                inventoryListView.Count = inventoryListView.Inventories.Count;

                //pagination
                if (inventoryListQuery.Page > 0 && inventoryListQuery.PageSize > 0 && inventoryListView.Inventories.Count > 0)
                {
                    inventoryListView.Inventories = inventoryListView.Inventories.InventoryPagination(inventoryListQuery);

                    if (inventoryListView.Inventories == null)
                        return Ok(new { count = inventoryListView.Count, message = $"There is no data to show for page {inventoryListQuery.Page}" });
                }

                //page in the response
                if (inventoryListQuery.Page > 0 && inventoryListQuery.PageSize > 0)
                    inventoryListView.Page = inventoryListQuery.Page;
                else
                    inventoryListView.Page = 1;

                return Ok(inventoryListView);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Inventory-list - Product: {ex.Message}");
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
