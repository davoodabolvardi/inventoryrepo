﻿using FluentValidation;
using Inventory.WebAPI.Contracts.EntryBill;
using System.Text.RegularExpressions;

namespace Inventory.WebAPI.Validations
{
    public class EntryBillValidator : AbstractValidator<EntryBillBody>
    {
        public EntryBillValidator()
        {
            RuleForEach(x => x.EntryBillDetails).ChildRules(entryBillDetail =>
            {
                entryBillDetail.RuleFor(x => x.ProductId).NotEmpty().NotNull();
                entryBillDetail.RuleFor(x => x.ProductId.ToString()).Must(z => IsValid(z)).WithMessage("ProductId is invalid");

                entryBillDetail.RuleFor(x => x.Count).NotNull().NotEmpty();
                entryBillDetail.RuleFor(x => x.Count.ToString()).Must(z => IsValid(z)).WithMessage("Count is invalid");
            });
        }

        public bool IsValid(string value)
        {
            Regex regex = new Regex(@"^[0-9]+$");
            if (string.IsNullOrWhiteSpace(value) || !regex.IsMatch(value))
                return false;

            return true;
        }
        
    }
}
