﻿using FluentValidation;
using Inventory.WebAPI.Contracts.Exception;
using Inventory.WebAPI.Contracts.Product;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Inventory.WebAPI.Validations
{
    public static class GeneralValidator
    {

        public static List<QueryException> InventoryListQueryValidator(this InventoryListQuery inventoryListQuery)
        {
            var queryException = new List<QueryException>();

            if (inventoryListQuery.Page > 0)
                if (!IsValid(inventoryListQuery.Page.ToString()))
                    queryException.Add(new QueryException { QueryTitle = "Page", Message = "Invalid page" });

            if (inventoryListQuery.Page < 0)
                queryException.Add(new QueryException { QueryTitle = "Page", Message = "Page must more than zero" });

            if (inventoryListQuery.PageSize > 0)
                if (!IsValid(inventoryListQuery.PageSize.ToString()))
                    queryException.Add(new QueryException { QueryTitle = "PageSize", Message = "Invalid PageSize" });

            if (inventoryListQuery.PageSize < 0)
                queryException.Add(new QueryException { QueryTitle = "PageSize", Message = "PageSize must more than zero" });

            if (inventoryListQuery.ProductId > 0)
                if (!IsValid(inventoryListQuery.ProductId.ToString()))
                    queryException.Add(new QueryException { QueryTitle = "ProductId", Message = "Invalid ProductId" });

            if (inventoryListQuery.ProductId <= 0)
                queryException.Add(new QueryException { QueryTitle = "ProductId", Message = "ProductId must more than zero" });

            if (inventoryListQuery.Inventory > 0)
                if (!IsValid(inventoryListQuery.Inventory.ToString()))
                    queryException.Add(new QueryException { QueryTitle = "Inventory", Message = "Invalid Inventory" });

            if (inventoryListQuery.Inventory <= 0)
                queryException.Add(new QueryException { QueryTitle = "Inventory", Message = "Inventory must more than zero" });

            if (inventoryListQuery.MinimumInventory > 0)
                if (!IsValid(inventoryListQuery.MinimumInventory.ToString()))
                    queryException.Add(new QueryException { QueryTitle = "MinimumInventory", Message = "Invalid MinimumInventory" });

            if (inventoryListQuery.MinimumInventory <= 0)
                queryException.Add(new QueryException { QueryTitle = "MinimumInventory", Message = "MinimumInventory must more than zero" });

            if (inventoryListQuery.Status != "آماده سفارش" && inventoryListQuery.Status != "ناموجود" && inventoryListQuery.Status != null)
                queryException.Add(new QueryException { QueryTitle = "Status", Message = "Invalid Status" });


            return queryException;
        }


        private static bool IsValid(string value)
        {
            Regex regex = new Regex(@"^[0-9]+$");
            if (string.IsNullOrWhiteSpace(value) || !regex.IsMatch(value.ToString()))
                return false;

            return true;
        }
    }
}
