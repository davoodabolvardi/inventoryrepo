﻿using FluentValidation;
using Inventory.WebAPI.Contracts.Product;
using System.Text.RegularExpressions;

namespace Inventory.WebAPI.Validations
{
    public class ProductBodyValidator : AbstractValidator<ProductBody>
    {
        public ProductBodyValidator()
        {
            RuleFor(x => x.Title).NotEmpty().NotNull().WithMessage("Title of product can not be null or empty.");
            RuleFor(x => x.CategoryId).NotEmpty().NotNull();
            RuleFor(x => x.CategoryId.ToString()).Must(m => IsValid(m)).WithMessage("CategoryId is invalid");
            RuleFor(x => x.MinimumInventory).NotEmpty().NotNull();
            RuleFor(x => x.MinimumInventory.ToString()).Must(m => IsValid(m)).WithMessage("MinimumInventory is invalid");
        }

        private bool IsValid(string value)
        {
            Regex regex = new Regex(@"^[0-9]+$");
            if (string.IsNullOrWhiteSpace(value) || !regex.IsMatch(value.ToString()))
                return false;

            return true;
        }
    }
}
