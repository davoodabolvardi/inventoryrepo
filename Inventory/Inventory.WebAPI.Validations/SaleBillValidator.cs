﻿using FluentValidation;
using Inventory.WebAPI.Contracts.SaleBill;
using System.Text.RegularExpressions;

namespace Inventory.WebAPI.Validations
{
    public class SaleBillValidator : AbstractValidator<SaleBillBody>
    {
        public SaleBillValidator()
        {
            RuleForEach(x => x.SaleBillDetailBodies).ChildRules(saleBillDetail =>
            {
                saleBillDetail.RuleFor(x => x.ProductId).NotEmpty().NotNull();
                saleBillDetail.RuleFor(x => x.ProductId.ToString()).Must(z => IsValid(z)).WithMessage("ProductId is invalid");

                saleBillDetail.RuleFor(x => x.Count).NotNull().NotEmpty();
                saleBillDetail.RuleFor(x => x.Count.ToString()).Must(z => IsValid(z)).WithMessage("Count is invalid");

                saleBillDetail.RuleFor(x => x.Amount).NotNull().NotEmpty();
                saleBillDetail.RuleFor(x => x.Amount.ToString()).Must(z => IsValid(z)).WithMessage("Amount is invalid");
            });
        }

        public bool IsValid(string value)
        {
            Regex regex = new Regex(@"^[0-9]+$");
            if (string.IsNullOrWhiteSpace(value) || !regex.IsMatch(value))
                return false;

            return true;
        }
    }
}
