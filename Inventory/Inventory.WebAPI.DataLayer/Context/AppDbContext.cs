﻿using Inventory.WebAPI.DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Inventory.WebAPI.DataLayer.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<EntryBill> EntryBills { get; set; }
        public DbSet<EntryBillDetail> EntryBillDetails { get; set; }
        public DbSet<SaleBill> SaleBills { get; set; }
        public DbSet<SaleBillDetail> SaleBillDetails { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserAddress> UserAddresses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<User>(new UserEntityMap());
            modelBuilder.ApplyConfiguration<UserAddress>(new UserAddressEntityMap());
            modelBuilder.ApplyConfiguration<Category>(new CategoryEntityMap());
            modelBuilder.ApplyConfiguration<Product>(new ProductEntityMap());
            modelBuilder.ApplyConfiguration<EntryBill>(new EntryBillEntityMap());
            modelBuilder.ApplyConfiguration<EntryBillDetail>(new EntryBillDetailEntityMap());
            modelBuilder.ApplyConfiguration<SaleBill>(new SaleBillEntityMap());
            modelBuilder.ApplyConfiguration<SaleBillDetail>(new SaleBillDetailEntityMap());
        }
    }

    #region EntityMaps
    public class UserEntityMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users")
                .HasKey(x => x.Id);

            builder.HasOne(x => x.UserAddress)
                .WithOne(x => x.User)
                .HasForeignKey<UserAddress>("UserId");

            builder.Property(p => p.Fullname).IsRequired().HasMaxLength(70);
            builder.Property(p => p.Gender).IsRequired().HasDefaultValue(true);
        }
    }

    public class UserAddressEntityMap : IEntityTypeConfiguration<UserAddress>
    {
        public void Configure(EntityTypeBuilder<UserAddress> builder)
        {
            builder.ToTable("UserAddresses")
                .HasKey(x => x.Id);
            
            //builder.HasOne(x => x.User)
            //    .WithOne(x => x.Address)
            //    .HasForeignKey("UserId");

            builder.Property(p => p.Address).HasMaxLength(200).IsRequired();
        }
    }

    public class CategoryEntityMap : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categories")
                .HasKey(x=>x.Id);

            builder.Property(p => p.Title).IsRequired().HasMaxLength(50);
        }
    }

    public class ProductEntityMap : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products")
                .HasKey(x => x.Id);

            builder.HasOne(x => x.Category)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.CategoryId);

            builder.Property(p => p.CategoryId).IsRequired();
            builder.Property(p => p.Title).IsRequired().HasMaxLength(100);
            builder.Property(p => p.MinimumInventory).IsRequired();
        }
    }

    public class EntryBillEntityMap : IEntityTypeConfiguration<EntryBill>
    {
        public void Configure(EntityTypeBuilder<EntryBill> builder)
        {
            builder.ToTable("EntryBills")
                .HasKey(x => x.Id);

            //builder.HasMany(x => x.EntryBillDetails)
            //    .WithOne(x => x.EntryBill)
            //    .HasForeignKey(x => x.EntryBillId);

            builder.Property(p => p.EntryBillNumber).IsRequired();
            builder.Property(p => p.Date).IsRequired();

        }
    }

    public class EntryBillDetailEntityMap : IEntityTypeConfiguration<EntryBillDetail>
    {
        public void Configure(EntityTypeBuilder<EntryBillDetail> builder)
        {
            builder.ToTable("EntryBillDetails")
                .HasKey(x => x.Id);

            builder.HasOne(x => x.EntryBill)
                .WithMany(x => x.EntryBillDetails)
                .HasForeignKey(x => x.EntryBillId);

            builder.HasOne(x => x.Product)
                .WithMany(x => x.EntryBillDetails).
                HasForeignKey(x => x.ProductId);

            builder.Property(p => p.EntryBillId).IsRequired();
            builder.Property(p => p.ProductId).IsRequired();
            builder.Property(p => p.Count).IsRequired();
        }
    }

   

    public class SaleBillEntityMap : IEntityTypeConfiguration<SaleBill>
    {
        public void Configure(EntityTypeBuilder<SaleBill> builder)
        {
            builder.ToTable("SaleBills")
                .HasKey(x => x.Id);

            builder.Property(p => p.SaleBillNumber).IsRequired();
            builder.Property(p => p.CustomerName).IsRequired().HasMaxLength(100);
            builder.Property(p => p.Date).IsRequired();
            builder.Property(p => p.AccountingDate).IsRequired();
            builder.Property(p => p.AccountingNumber).IsRequired();
        }
    }

    public class SaleBillDetailEntityMap : IEntityTypeConfiguration<SaleBillDetail>
    {
        public void Configure(EntityTypeBuilder<SaleBillDetail> builder)
        {
            builder.ToTable("SaleBillDetails")
                .HasKey(x => x.Id);

            builder.HasOne(x => x.SaleBill)
                .WithMany(x => x.SaleBillDetails)
                .HasForeignKey(x => x.SaleBillId);

            builder.HasOne(x => x.Product)
                .WithMany(x => x.SaleBillDetails)
                .HasForeignKey(x => x.ProductId);

            builder.Property(p => p.ProductId).IsRequired();
            builder.Property(p => p.SaleBillId).IsRequired();
            builder.Property(p => p.Count).IsRequired();
            builder.Property(p => p.Amount).IsRequired();
        }
    }

    #endregion


}
