﻿namespace Inventory.WebAPI.DataLayer.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public byte Gender { get; set; }

        public UserAddress UserAddress { get; set; }

    }
}
