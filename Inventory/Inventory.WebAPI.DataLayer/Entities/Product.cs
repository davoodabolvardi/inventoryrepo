﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inventory.WebAPI.DataLayer.Entities
{
    public class Product
    {
        public Product()
        {
            EntryBillDetails = new List<EntryBillDetail>();
            SaleBillDetails = new List<SaleBillDetail>();
        }
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public string Title { get; set; }
        
        public int MinimumInventory { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<EntryBillDetail> EntryBillDetails { get; set; }
        public virtual ICollection<SaleBillDetail> SaleBillDetails { get; set; }
    }
}
