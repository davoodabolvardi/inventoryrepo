﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.WebAPI.DataLayer.Entities
{
    public class UserAddress
    {
        public int Id { get; set; }
        public string Address { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }

    }
}
