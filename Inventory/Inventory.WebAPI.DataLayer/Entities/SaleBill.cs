﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inventory.WebAPI.DataLayer.Entities
{
    public class SaleBill
    {
        public SaleBill()
        {
            SaleBillDetails = new List<SaleBillDetail>();
        }
        public int Id { get; set; }

        public int SaleBillNumber { get; set; }

        public string CustomerName { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public int AccountingNumber { get; set; }

        public DateTime AccountingDate { get; set; } = DateTime.Now;

        public virtual ICollection<SaleBillDetail> SaleBillDetails { get; set; }
    }
}
