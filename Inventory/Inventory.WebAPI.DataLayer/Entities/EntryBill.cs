﻿using System;
using System.Collections.Generic;

namespace Inventory.WebAPI.DataLayer.Entities
{
    public class EntryBill
    {
        public EntryBill()
        {
            EntryBillDetails = new List<EntryBillDetail>();
        }

        public int Id { get; set; }

        public int EntryBillNumber { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public virtual ICollection<EntryBillDetail> EntryBillDetails { get; set; }
    }
}
