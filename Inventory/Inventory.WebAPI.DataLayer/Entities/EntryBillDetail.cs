﻿namespace Inventory.WebAPI.DataLayer.Entities
{
    public class EntryBillDetail
    {
        public int Id { get; set; }

        public int EntryBillId { get; set; }

        public int ProductId { get; set; }

        public int Count { get; set; }

        public virtual EntryBill EntryBill { get; set; }
        public virtual Product Product { get; set; }
    }
}
