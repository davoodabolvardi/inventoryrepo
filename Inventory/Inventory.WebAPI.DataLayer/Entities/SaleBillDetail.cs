﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inventory.WebAPI.DataLayer.Entities
{
    public class SaleBillDetail
    {
        public int Id { get; set; }

        public int SaleBillId { get; set; }

        public int ProductId { get; set; }

        public int Count { get; set; }

        public int Amount { get; set; }

        public virtual SaleBill SaleBill { get; set; }
        public virtual Product Product { get; set; }
    }
}
