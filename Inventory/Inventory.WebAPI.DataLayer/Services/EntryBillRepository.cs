﻿using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.Repositories;

namespace Inventory.WebAPI.DataLayer.Services
{
    public class EntryBillRepository : Repository<EntryBill>, IEntryBillRepository
    {
        public EntryBillRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
