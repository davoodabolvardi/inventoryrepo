﻿using Inventory.WebAPI.Contracts.SaleBill;
using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Inventory.WebAPI.DataLayer.Services
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        private readonly AppDbContext _dbContext;
        public ProductRepository(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public string GetProductStatus(int productId)
        {
            var sumEntryBillOfProduct = CalculateEntryBillProduct(productId);
            
            var sumSaleBillOfProduct = CalculateSaleBillProduct(productId);

            var minimumInventoryProduct = CalculateMinimumInventroy(productId);

            if ((sumEntryBillOfProduct - sumSaleBillOfProduct) > minimumInventoryProduct)
                return "آماده سفارش";
            else
                return "ناموجود";
        }

        public SaleBillProduct MinimumInventoryIsValid(int productId, int count)
        {
            var saleBillProduct = new SaleBillProduct();
            var product = _dbContext.Products.FirstOrDefault(z => z.Id == productId);
            var sumMinimumInventory = _dbContext.Products.Where(z => z.Id == productId).Sum(z => z.MinimumInventory);
            saleBillProduct.ProductTitle = product.Title;

            if (sumMinimumInventory < count)
                saleBillProduct.MinInventoryIsValid = false;
            else
                saleBillProduct.MinInventoryIsValid = true;

            return saleBillProduct;
        }

        public int CalculateInventory(int productId)
        {
            var sumEntryBillOfProduct = CalculateEntryBillProduct(productId);

            var sumSaleBillOfProduct = CalculateSaleBillProduct(productId);

            return sumEntryBillOfProduct - sumSaleBillOfProduct;
        }

        private int CalculateEntryBillProduct(int productId)
        {
            return _dbContext.EntryBillDetails.Where(z => z.ProductId == productId).Sum(z => z.Count);
        }

        private int CalculateSaleBillProduct(int productId)
        {
            return _dbContext.SaleBillDetails.Where(z => z.ProductId == productId).Sum(z => z.Count);
        }

        private int CalculateMinimumInventroy(int productId)
        {
            return _dbContext.Products.FirstOrDefault(z => z.Id == productId).MinimumInventory;
        }

        public string GetCategoryName(int productId)
        {
            return _dbContext.Products.Include("Category").FirstOrDefault(z => z.Id == productId).Category.Title;
        }
    }
}
