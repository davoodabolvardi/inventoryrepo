﻿using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.Repositories;

namespace Inventory.WebAPI.DataLayer.Services
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
