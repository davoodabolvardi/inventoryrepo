﻿using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.WebAPI.DataLayer.Services
{
    class SaleBillDetailRepository : Repository<SaleBillDetail>, ISaleBillDetailRepository
    {
        public SaleBillDetailRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
