﻿using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.Repositories;

namespace Inventory.WebAPI.DataLayer.Services
{
    public class EntryBillDetailRepository : Repository<EntryBillDetail> , IEntryBillDetailRepository
    {
        public EntryBillDetailRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
