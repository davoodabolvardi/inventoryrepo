﻿using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.Entities;
using Inventory.WebAPI.DataLayer.Repositories;

namespace Inventory.WebAPI.DataLayer.Services
{
    public class SaleBillRepository : Repository<SaleBill>, ISaleBillRepository
    {
        public SaleBillRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
