﻿using Inventory.WebAPI.DataLayer.Context;
using Inventory.WebAPI.DataLayer.Repositories;
using Inventory.WebAPI.DataLayer.Services;
using System;

namespace Inventory.WebAPI.DataLayer.UnitOfWorks
{
    public class UnitOfWork : IDisposable
    {
        private readonly AppDbContext _dbContext;

        public UnitOfWork(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private ICategoryRepository _categoryRepository;
        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_dbContext);
                }
                return _categoryRepository;
            }
        }

        private IProductRepository _productRepository;
        public IProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                {
                    _productRepository = new ProductRepository(_dbContext);
                }
                return _productRepository;
            }
        }

        private IEntryBillRepository _entryBillRepository;
        public IEntryBillRepository EntryBillRepository
        {
            get
            {
                if (_entryBillRepository == null)
                {
                    _entryBillRepository = new EntryBillRepository(_dbContext);
                }
                return _entryBillRepository;
            }
        }

        private IEntryBillDetailRepository _entryBillDetailRepository;
        public IEntryBillDetailRepository EntryBillDetailRepository
        {
            get
            {
                if (_entryBillDetailRepository == null)
                {
                    _entryBillDetailRepository = new EntryBillDetailRepository(_dbContext);
                }
                return _entryBillDetailRepository;
            }
        }

        private ISaleBillRepository _saleBillRepository;
        public ISaleBillRepository SaleBillRepository
        {
            get
            {
                if (_saleBillRepository == null)
                {
                    _saleBillRepository = new SaleBillRepository(_dbContext);
                }
                return _saleBillRepository;
            }
        }

        private ISaleBillDetailRepository _saleBillDetailRepository;
        public ISaleBillDetailRepository SaleBillDetailRepository
        {
            get
            {
                if (_saleBillDetailRepository == null)
                {
                    _saleBillDetailRepository = new SaleBillDetailRepository(_dbContext);
                }
                return _saleBillDetailRepository;
            }
        }


        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
