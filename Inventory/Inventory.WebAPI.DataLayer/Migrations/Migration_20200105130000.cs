﻿using FluentMigrator;

namespace Inventory.WebAPI.DataLayer.Migrations
{
    [Migration(20200105130000)]
    public class Migration_20200105130000 : Migration
    {
        public override void Up()
        {
            Execute.Script(@"C:\scripts\ScriptMigration2.sql");
        }
        public override void Down()
        {
            Execute.Script(@"C:\scripts\ScriptMigration-remove.sql");
        }

    }
}
