﻿using FluentMigrator;

namespace Inventory.WebAPI.DataLayer.Migrations
{
    [Migration(20200105110101)]
    public class Migration_20200105110101 : Migration
    {
        public override void Down()
        {
            Delete.Column("Gender").FromTable("Users");
        }

        public override void Up()
        {
            Alter.Table("Users").AddColumn("Gender").AsBoolean().NotNullable().WithDefaultValue(true);
        }
    }
}
