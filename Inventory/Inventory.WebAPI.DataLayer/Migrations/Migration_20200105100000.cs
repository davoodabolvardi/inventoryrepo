﻿using FluentMigrator;
using FluentMigrator.SqlServer;

namespace Inventory.WebAPI.DataLayer.Migrations
{
    [Migration(20200105100000)]
    public class Migration_20200105100000 : Migration
    {
        public override void Down()
        {
            Delete.Table("UserAddresses");
            Delete.Table("Users");
        }

        public override void Up()
        {
            Create.Table("Users")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("Fullname").AsString(75).NotNullable()
                .WithColumn("NationalCode").AsCustom("char(10)").NotNullable()
                .WithColumn("Email").AsCustom("varchar(100)").NotNullable();

            Create.Table("UserAddresses")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("Address").AsString(300).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable().ForeignKey("Users", "Id");

        }
    }
}