﻿using FluentMigrator;
using FluentMigrator.SqlServer;
using System;

namespace Inventory.WebAPI.DataLayer.Migrations
{
    [Migration(20200104103311)]
    public class Migration_20200104103311 : Migration
    {
        public override void Down()
        {
            Delete.Table("EntryBillDetails");
            Delete.Table("EntryBills");
            Delete.Table("SaleBillDetails");
            Delete.Table("SaleBills");
            Delete.Table("Products");
            Delete.Table("Categories");
        }

        public override void Up()
        {
            Create.Table("Categories")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("Title").AsString(50).NotNullable();

            Create.Table("Products")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("CategoryId").AsInt32().NotNullable().ForeignKey("Categories", "Id")
                .WithColumn("Title").AsString(50).NotNullable()
                .WithColumn("MinimumInventory").AsInt32().NotNullable();

            Create.Index("IX_Products_CategoryId")
                .OnTable("Products")
                .OnColumn("CategoryId")
                .Ascending()
                .WithOptions().NonClustered();

            Create.Table("EntryBills")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("EntryBillNumber").AsInt32().NotNullable()
                .WithColumn("Date").AsDateTime().NotNullable();

            Create.Table("EntryBillDetails")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("EntryBillId").AsInt32().NotNullable().ForeignKey("EntryBills", "Id")
                .WithColumn("ProductId").AsInt32().NotNullable().ForeignKey("Products", "Id")
                .WithColumn("Count").AsInt32().NotNullable();

            Create.Index("IX_EntryBillDetails_EntryBillId")
                .OnTable("EntryBillDetails")
                .OnColumn("EntryBillId")
                .Ascending()
                .WithOptions().NonClustered();

            Create.Index("IX_EntryBillDetails_ProductId")
                .OnTable("EntryBillDetails")
                .OnColumn("ProductId")
                .Ascending()
                .WithOptions().NonClustered();

            Create.Table("SaleBills")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("SaleBillNumber").AsInt32().NotNullable()
                .WithColumn("CustomerName").AsString(100).NotNullable()
                .WithColumn("Date").AsDateTime().NotNullable()
                .WithColumn("AccountingNumber").AsInt32().Nullable()
                .WithColumn("AccountingDate").AsDateTime().NotNullable();

            Create.Table("SaleBillDetails")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity(1, 1)
                .WithColumn("SaleBillId").AsInt32().NotNullable().ForeignKey("SaleBills","Id")
                .WithColumn("ProductId").AsInt32().NotNullable().ForeignKey("Products","Id")
                .WithColumn("Count").AsInt32().NotNullable()
                .WithColumn("Amount").AsInt32().NotNullable();

            Create.Index("IX_SaleBillDetails_SaleBillId")
                .OnTable("SaleBillDetails")
                .OnColumn("SaleBillId")
                .Ascending()
                .WithOptions().NonClustered();

            Create.Index("IX_SaleBillDetails_ProductId")
                .OnTable("SaleBillDetails")
                .OnColumn("ProductId")
                .Ascending()
                .WithOptions().NonClustered();
        }
       
    }
}
