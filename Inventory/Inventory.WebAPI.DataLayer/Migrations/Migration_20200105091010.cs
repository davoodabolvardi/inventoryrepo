﻿using FluentMigrator;
using System;
using FluentMigrator.SqlServer;

namespace Inventory.WebAPI.DataLayer.Migrations
{
    [Migration(20200105091010)]
    public class Migration_20200105091010 : Migration
    {
        public override void Down()
        {
            Alter.Column("Title")
                .OnTable("Products")
                .AsString(50);
        }

        public override void Up()
        {
            Alter.Column("Title")
                .OnTable("Products")
                .AsString(100);
        }
    }
}
