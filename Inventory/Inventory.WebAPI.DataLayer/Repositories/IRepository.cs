﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Inventory.WebAPI.DataLayer.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> expression = null);
        public TEntity GetById(object id);
        TEntity Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(object id);
        void SaveChanges();
    }
}
