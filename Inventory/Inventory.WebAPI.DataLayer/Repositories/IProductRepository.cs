﻿using Inventory.WebAPI.Contracts.SaleBill;
using Inventory.WebAPI.DataLayer.Entities;

namespace Inventory.WebAPI.DataLayer.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        SaleBillProduct MinimumInventoryIsValid(int productId, int count);
        string GetProductStatus(int productId);
        int CalculateInventory(int productId);
        string GetCategoryName(int productId);
    }
}
