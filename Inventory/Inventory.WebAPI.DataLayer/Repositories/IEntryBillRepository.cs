﻿using Inventory.WebAPI.DataLayer.Entities;

namespace Inventory.WebAPI.DataLayer.Repositories
{
    public interface IEntryBillRepository :IRepository<EntryBill>
    {
    }
}
