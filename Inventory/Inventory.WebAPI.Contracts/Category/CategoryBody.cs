﻿namespace Inventory.WebAPI.Contracts.Category
{
    public class CategoryBody
    {
        public string Title { get; set; }
    }
}
