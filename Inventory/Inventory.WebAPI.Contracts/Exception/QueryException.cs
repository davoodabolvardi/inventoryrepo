﻿namespace Inventory.WebAPI.Contracts.Exception
{
    public class QueryException
    {
        public string QueryTitle { get; set; }
        public string Message { get; set; }
    }
}
