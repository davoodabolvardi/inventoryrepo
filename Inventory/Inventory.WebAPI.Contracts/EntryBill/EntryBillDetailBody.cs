﻿namespace Inventory.WebAPI.Contracts.EntryBill
{
    public class EntryBillDetailBody
    {
        public int ProductId { get; set; }
        public int Count { get; set; }
    }
}
