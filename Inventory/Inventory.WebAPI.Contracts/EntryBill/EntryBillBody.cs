﻿using System.Collections.Generic;

namespace Inventory.WebAPI.Contracts.EntryBill
{
    public class EntryBillBody
    {
        public List<EntryBillDetailBody> EntryBillDetails { get; set; }
    }
}
