﻿namespace Inventory.WebAPI.Contracts.SaleBill
{
    public class SaleBillView
    {
        public string ProductName { get; set; }
        public string Message { get; set; }
    }
}
