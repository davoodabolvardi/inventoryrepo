﻿using System.Collections.Generic;

namespace Inventory.WebAPI.Contracts.SaleBill
{
    public class SaleBillBody
    {
        public string CustomerName { get; set; }
        public List<SaleBillDetailBody> SaleBillDetailBodies { get; set; }
    }
}
