﻿namespace Inventory.WebAPI.Contracts.SaleBill
{
    public class SaleBillDetailBody
    {
        public int ProductId { get; set; }
        public int Count { get; set; }
        public int Amount { get; set; }
    }
}
