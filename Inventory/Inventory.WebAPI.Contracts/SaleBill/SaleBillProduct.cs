﻿namespace Inventory.WebAPI.Contracts.SaleBill
{
    public class SaleBillProduct
    {
        public string ProductTitle { get; set; }
        public bool MinInventoryIsValid { get; set; }
    }
}
