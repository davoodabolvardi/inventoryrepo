﻿namespace Inventory.WebAPI.Contracts.Product
{
    public class InventoryListQuery : InventoryItem
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string WhichOneToSort { get; set; }
        public bool Descending { get; set; }
    }
}
