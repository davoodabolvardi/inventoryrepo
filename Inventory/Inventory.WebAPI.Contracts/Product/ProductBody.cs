﻿namespace Inventory.WebAPI.Contracts.Product
{
    public class ProductBody
    {
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public int MinimumInventory { get; set; }
    }
}
