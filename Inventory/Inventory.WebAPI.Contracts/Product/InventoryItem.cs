﻿namespace Inventory.WebAPI.Contracts.Product
{
    public class InventoryItem
    {
        public int? ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string CategoryName { get; set; }
        public int? Inventory { get; set; }
        public int? MinimumInventory { get; set; }
        public string Status { get; set; }
    }
}
