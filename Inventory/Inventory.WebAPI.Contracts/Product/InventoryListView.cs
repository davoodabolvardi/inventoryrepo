﻿using System.Collections.Generic;

namespace Inventory.WebAPI.Contracts.Product
{
    public class InventoryListView
    {
        public InventoryListView()
        {
            Inventories = new List<InventoryItem>();
        }
        public int Count { get; set; }
        public int Page { get; set; }
        public List<InventoryItem> Inventories { get; set; }
    }
}
